# JavaWebLearning

#### 介绍
·java学习ing
·用jsp+h5+css+js基本构建一个简单的web项目

#### 软件架构
软件架构说明


#### 安装教程

1. 此maven项目通过idea安装，运行前需要配置tomcat，此项目用的是tomcat 9

   ![image-20211021130218726](D:\Git_can\img\image-20211021130218726.png)

2.  在resources/db.properties中配置基本的数据库文件及数据库驱动，链接数据库，目录处.sql文件为数据库配置文件

    ![image-20211021130506827](D:\Git_can\img\image-20211021130506827.png)

3.  配置完成后即可开始运行

    ![image-20211021130632881](D:\Git_can\img\image-20211021130632881.png)

#### 使用说明

1. 管理员界面

   <img src="D:\Git_can\img\image-20211021132440835.png" alt="image-20211021132440835" style="zoom: 80%;" />

2. 老师界面

   ![image-20211021132544042](D:\Git_can\img\image-20211021132544042.png)

3. 学生界面

   ![image-20211021132604706](D:\Git_can\img\image-20211021132604706.png)

#### 不足和方向

1.此项目对原有的项目进行了一些个人的改进，并非独立完成且完善了所有的功能，只能说重构了部分代码

2.项目仍然存在bug，例如切换身份登录后，名称没有及时的转换，时间栏下面的鸡汤结尾有个奇怪的问号，取消和返回按钮没有作用。

3.下一步打算学习一下servlet源码，再进行springboot的学习。
