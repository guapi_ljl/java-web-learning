package com.chooseCourses.pojo;

import java.util.Date;

public class schedule {
    private Integer id;
    private Integer studentid;
    private Integer teacherid;
    private Date time;
    private String room;

    @Override
    public String toString() {
        return "schedule{" +
                "id=" + id +
                ", studentid=" + studentid +
                ", teacherid=" + teacherid +
                ", time=" + time +
                ", room='" + room + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentid() {
        return studentid;
    }

    public void setStudentid(Integer studentid) {
        this.studentid = studentid;
    }

    public Integer getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Integer teacherid) {
        this.teacherid = teacherid;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }
}
