package com.chooseCourses.servlet.student;

import com.chooseCourses.dao.BaseDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet("/StudentModified")
public class StudentModified extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().append("Served at: ").append(req.getContextPath());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("userName");
        String userId = req.getParameter("userId");
        String oldPw = req.getParameter("oldPw");
        String newPw = req.getParameter("newPw");

        BaseDao baseDao = new BaseDao();
        //判断旧密码是否与新密码一致
        String sql = "select * from student where id='" + userId + "' and loginpassword='" + oldPw + "'";
        //创建更新sql语句
        String upSql = "update student set loginpassword = ? where id= ?";

        //创建一个rs结果集,ps对象
        PreparedStatement ps;
        ResultSet rs;
        //调用db中的查询方法
        try {
            ps = baseDao.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();//执行语句
            //判断旧密码是否存在,rs.next()返回的是布尔值
            if(rs.next()) {//存在,则执行更新操作
                //调用更新操作
                ps = baseDao.getConnection().prepareStatement(upSql);
                //要更新的数据
                ps.setString(1, newPw);
                ps.setString(2, userId);
                ps.executeUpdate();//执行更新
                //成功返回管理员信息页面,这里可以设置一个提示页面再跳,不设置也可以
                PrintWriter out = resp.getWriter();
                resp.sendRedirect("student/studentMes.jsp?userName=" + userName + "&userId=" + userId);
            }else {//不存在,则跳回修改页面,并提示错误
//                PrintWriter out = resp.getWriter();
                req.setAttribute("message","旧密码不正确，无法设置密码，请重新输入");
                req.getRequestDispatcher("student/StudentModified.jsp?userName=" + userName + "&userId=" + userId).forward(req,resp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        doGet(req, resp);
    
    }

    public StudentModified() {
        super();
    }
}
