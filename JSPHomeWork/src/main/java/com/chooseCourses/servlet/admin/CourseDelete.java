package com.chooseCourses.servlet.admin;

import com.chooseCourses.dao.BaseDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/CourseDelete")
public class CourseDelete extends HttpServlet {
    public CourseDelete() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));  //获传过来的值
        String userName = req.getParameter("userName");
        int userId = Integer.parseInt(req.getParameter("userId"));
        //创建DB对象
        BaseDao baseDao = new BaseDao();
        String sql = "delete from course where id=" + id;//创建sql语句
        //调用删除操作
        baseDao.execute(sql);
        //关闭数据流
        try {
            baseDao.closeResource(null,null,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PrintWriter out = resp.getWriter();
        resp.sendRedirect("admin/courseMes.jsp?userName=" + userName + "&userId=" + userId);
        resp.getWriter().append("Served at: ").append(req.getContextPath());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
