package com.chooseCourses.servlet.admin;


import com.chooseCourses.dao.BaseDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/TeacherDelete")
public class TeacherDelete extends HttpServlet {
    public TeacherDelete() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String deleteId = req.getParameter("deleteId");
        String userName = req.getParameter("userName");
        String userId = req.getParameter("userId");

        BaseDao baseDao = new BaseDao();
        String sql="delete from teacher where id=" + deleteId;
        try {
            baseDao.execute(sql);
            baseDao.closeResource(null,null,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.sendRedirect("admin/teacherMes.jsp?userName=" + userName + "&userId=" + userId);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
