package com.chooseCourses.servlet.admin;

import com.chooseCourses.dao.BaseDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ScheduleDelete")
public class ScheduleDelete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int deleteId = Integer.parseInt(req.getParameter("deleteId"));  //获传过来的值
        String userName = req.getParameter("userName");
        int userId = Integer.parseInt(req.getParameter("userId"));
        //创建DB对象
        BaseDao baseDao = new BaseDao();

        String sql = "delete from schedule where id =" + deleteId;//创建sql语句
        //调用删除操作
        baseDao.execute(sql);
        //关闭数据流
        try {
            baseDao.closeResource(null,null,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.sendRedirect("admin/scheduleMes.jsp?userName=" + userName + "&userId=" + userId);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    public ScheduleDelete() {super();
    }
}
