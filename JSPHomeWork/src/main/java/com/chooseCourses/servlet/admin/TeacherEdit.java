package com.chooseCourses.servlet.admin;

import com.chooseCourses.dao.BaseDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;

@WebServlet("/TeacherEdit")
public class TeacherEdit extends HttpServlet {
    public TeacherEdit() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().append("Served at: ").append(req.getContextPath());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("userId");
        String userName = req.getParameter("userName");
        String editId=req.getParameter("editId");
        int id = Integer.parseInt(req.getParameter("id"));
        String name = req.getParameter("name");
        String sex = req.getParameter("sex");
        String age = req.getParameter("age");
        String technical = req.getParameter("technical");
        String loginid = req.getParameter("loginid");
        String loginpassword = req.getParameter("loginpassword");

        //创建db对象
        BaseDao baseDao = new BaseDao();
        PreparedStatement ps = null;

        //创建更新sql语句
        String sql = "update teacher set id=?,name=?,sex=?,age=?,technical=?,loginid=?,loginpassword=? where id=?";
        //调用更新操作
        //要更新的数据
        try {
            ps = baseDao.getConnection().prepareStatement(sql);

            ps.setInt(1, id);
            ps.setString(2, name);
            ps.setString(3, sex);
            ps.setString(4, age);
            ps.setString(5, technical);
            ps.setString(6, loginid);
            ps.setString(7, loginpassword);
            ps.setString(8, editId);

            ps.executeUpdate();//执行更新
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            baseDao.closeResource(null,ps,null);//关闭数据流
        } catch (Exception e) {
            e.printStackTrace();
        }
        //成功返回管理员信息页面,这里可以设置一个提示页面再跳,不设置也可以
        resp.sendRedirect("admin/teacherMes.jsp?userName=" + userName + "&userId=" + userId);
        doGet(req, resp);

    }
}
