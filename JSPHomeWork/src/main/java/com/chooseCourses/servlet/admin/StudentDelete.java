package com.chooseCourses.servlet.admin;

import com.chooseCourses.dao.BaseDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/StudentDelete")
public class StudentDelete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().append("Served at: ").append(req.getContextPath());

        int deleteId = Integer.parseInt(req.getParameter("deleteId"));  //获传过来的值
        String userName = req.getParameter("userName");
        int userId = Integer.parseInt(req.getParameter("userId"));
        //创建DB对象
        BaseDao baseDao = new BaseDao();
        String sql = "delete from student where id=" + deleteId;//创建sql语句
        //调用删除操作
        baseDao.execute(sql);
        //关闭数据流
        try {
            baseDao.closeResource(baseDao.getConnection(),null,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        PrintWriter out = resp.getWriter();
        resp.sendRedirect("admin/studentMes.jsp?userName=" + userName + "&userId=" + userId);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    public StudentDelete() {
        super();

    }
}
