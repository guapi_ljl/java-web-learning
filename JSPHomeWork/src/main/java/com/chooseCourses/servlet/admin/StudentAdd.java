package com.chooseCourses.servlet.admin;

import com.chooseCourses.dao.BaseDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;

@WebServlet("/StudentAdd")
public class StudentAdd extends HttpServlet {
    public StudentAdd() {super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().append("Served at: ").append(req.getContextPath());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String sex = req.getParameter("sex");
        String age = req.getParameter("age");
        String classes = req.getParameter("classes");
        String loginid = req.getParameter("loginid");
        String loginpassword = req.getParameter("loginpassword");

        String userId = req.getParameter("userId");
        String userName = req.getParameter("userName");

        BaseDao baseDao = new BaseDao();
        PreparedStatement preparedStatement = null;
        String sql="insert into student(id,name,sex,age,classes,loginid,loginpassword) values(?,?,?,?,?,?,?)";
        try {
            preparedStatement = baseDao.getConnection().prepareStatement(sql);
            preparedStatement.setString(1,id);
            preparedStatement.setString(2,name);
            preparedStatement.setString(3,sex);
            preparedStatement.setString(4,age);
            preparedStatement.setString(5,classes);
            preparedStatement.setString(6,loginid);
            preparedStatement.setString(7,loginpassword);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            baseDao.closeResource(null,preparedStatement,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.sendRedirect("admin/studentMes.jsp?userName=" + userName + "&userId=" + userId);
        doGet(req, resp);
    }
}
