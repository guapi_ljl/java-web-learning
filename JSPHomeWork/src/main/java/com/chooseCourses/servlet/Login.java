package com.chooseCourses.servlet;

import com.chooseCourses.dao.BaseDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet(urlPatterns = "/Login")
public class Login extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public Login() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().append("Served at: ").append(req.getContextPath());

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String loginId = req.getParameter("loginId");
        String loginPassword = req.getParameter("loginPassword");
        String chose = req.getParameter("chose");

        //System.out.println("this is chose in login :"+chose);
        String username=null;
        BaseDao baseDao = new BaseDao();
        Connection connection;
        PreparedStatement ps;//创建ps对象
        ResultSet rs;//创建rs对象,结果集
        //      判断管理员登录

        if ("admin".equals(chose)) {
            int userId = 0;
            String sql = "select * from admin where loginid = '" + loginId + "' and loginpassword = '" + loginPassword + "'";
            try {
                connection = baseDao.getConnection();
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    userId = rs.getInt("id");
                    username = rs.getString("name");
                }
                if (userId != 0) {
//                    req.setAttribute("userId", userId);
//                    req.setAttribute("userName", username);
//                    req.setAttribute("chose",chose);


                    resp.addCookie( new Cookie("userName",username));
                    resp.addCookie(new Cookie("userId", Integer.toString(userId)));
                    resp.addCookie(new Cookie("chose", chose));

                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("admin/index.jsp");

                    requestDispatcher.forward(req, resp);
                    return;
                } else {
                    baseDao.closeResource(connection, ps, rs);
                    //关闭数据库操作
                    req.setAttribute("message", "请检查你的用户名或密码是否正确,必要时可联系管理员");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("login.jsp");    // 使用req对象获取RequestDispatcher对象
                    dispatcher.forward(req, resp);
                    return;
                }

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
            }

        }

        if ("student".equals(chose)) {
            int userId = 0;
            String sql = "select * from student where loginid = '" + loginId + "' and loginpassword = '" + loginPassword + "'";
            try {
                connection = baseDao.getConnection();
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    userId = rs.getInt("id");
                    username = rs.getString("name");
                }
                if (userId != 0) {
                    resp.addCookie( new Cookie("userName",username));
                    resp.addCookie(new Cookie("userId", Integer.toString(userId)));
                    resp.addCookie(new Cookie("chose", chose));

                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("student/index.jsp");
                    requestDispatcher.forward(req, resp);
                    return;
                } else {
                    baseDao.closeResource(connection, ps, rs);
                    //关闭数据库操作
                    req.setAttribute("message", "请检查你的用户名或密码是否正确,必要时可联系管理员");
                    RequestDispatcher dispatcher = req.getRequestDispatcher("login.jsp");    // 使用req对象获取RequestDispatcher对象
                    dispatcher.forward(req, resp);
                    return;
                }

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
            }

        }

        if ("teacher".equals(chose)) {
            int userId = 0;
            String sql = "select * from teacher where loginid = '" + loginId + "' and loginpassword = '" + loginPassword + "'";
            try {
                connection = baseDao.getConnection();
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    userId = rs.getInt("id");
                    username = rs.getString("name");

                }
                if (userId != 0) {
                    resp.addCookie( new Cookie("userName",username));
                    resp.addCookie(new Cookie("userId", Integer.toString(userId)));
                    resp.addCookie(new Cookie("chose", chose));

                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("teacher/index.jsp");
                    requestDispatcher.forward(req, resp);
                    return;
                } else {
                    baseDao.closeResource(connection, ps, rs);
                    //关闭数据库操作
                    req.setAttribute("message", "请检查你的用户名或密码是否正确,必要时可联系管理员");

                    RequestDispatcher dispatcher = req.getRequestDispatcher("login.jsp");    // 使用req对象获取RequestDispatcher对象
                    dispatcher.forward(req, resp);
                    return;
                }

            } catch (Exception sqlException) {
                sqlException.printStackTrace();
            }

        }


        doGet(req, resp);
    }
}
