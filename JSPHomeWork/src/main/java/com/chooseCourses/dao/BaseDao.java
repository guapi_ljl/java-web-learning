package com.chooseCourses.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class BaseDao {
    private static final String driver;
    private static final String url;
    private static final String username;
    private static final String password;
    private Connection connection;//connection
    private PreparedStatement preparedStatement;//预处理
    private ResultSet resultSet;//resultset
    private Statement statement;//状态？

    //    静态代码块，类加载时执行
    static {
        Properties properties = new Properties();
        String configFile = "db.properties";
        InputStream is = BaseDao.class.getClassLoader().getResourceAsStream(configFile);
        try {
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        driver = properties.getProperty("driver");
        url = properties.getProperty("url");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
    }

    //  链接数据库
    public Connection getConnection() throws Exception {

        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            System.out.println("数据库驱动加载失败");
            e.printStackTrace();
        }
        return connection;
    }

    //  用sql语句获取数据库内容
    public ResultSet execute(Connection connection, PreparedStatement pstm, ResultSet resultSet, String sql, Object[] params) throws Exception {
        try {
            pstm = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                Object param = params[i];
                pstm.setObject(i + 1, param);
            }
            resultSet = pstm.executeQuery();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return resultSet;
    }

    //    修改
    public int execute(String sql) {
        Connection connection = null;
        try {
            connection = getConnection();
            connection.createStatement().executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

    public int execute(Connection connection, PreparedStatement pstm, String sql, Object[] params) throws Exception {
        int updateRows = 0;
        try {
            pstm = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                Object param = params[i];
                pstm.setObject(i + 1, param);
            }
            updateRows = pstm.executeUpdate();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return updateRows;
    }

    //    数据库分页
    public ResultSet getPage(String sql, int begin, int size) throws Exception {
        String s = " limit " + begin + "," + size;
        sql = sql + s;
        try {
            statement = getConnection().createStatement();//创建statement对象
            resultSet = statement.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    //    关闭资源
    public boolean closeResource(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet) throws Exception {
        boolean flag = true;

        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
                flag = false;
            }
        }
        if (resultSet != null) {
            try {
                resultSet.close();
                resultSet = null;
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
                flag = false;
            }
        }
        if (preparedStatement != null) {
            try {
                preparedStatement.close();
                preparedStatement = null;
            } catch (SQLException sqlException) {
                sqlException.printStackTrace();
                flag = false;
            }
        }
        return flag;
    }
}
