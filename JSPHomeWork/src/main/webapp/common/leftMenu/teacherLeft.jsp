<%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/16
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Cookie[] cookies = request.getCookies();
    for (Cookie cookie : cookies) {
        if (cookie.getName().equals("userName")){
            request.setAttribute("userName",cookie.getValue());
        }
        if(cookie.getName().equals("userId")){
            request.setAttribute("userId",cookie.getValue());
        }
        if (cookie.getName().equals("userChose")){
            request.setAttribute("userChose",cookie.getValue());
        }
    }
%>
<div class="leftMeun" id="leftMeun">
    <div id="logoDiv">
        <p id="logoP">
            <img id="logo" src="${pageContext.request.contextPath}/images/logo2.png"><span>${userName}(教师)</span>
        </p>
    </div>
    <div id="personInfor">
        <p id="showtime"></p>
        <p id="zn"></p>
        <p>
            <a href="${pageContext.request.contextPath}/exit.jsp">退出登录</a>
        </p>
    </div>
    <div class="meun-title">个人信息</div>
    <div class="meun-item" >
        <a href="${pageContext.request.contextPath}/teacher/teacherMes.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_card_grey.png">我的信息</a>
    </div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/teacher/teacherEdit.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_rule_grey.png">修改密码</a>
    </div>
    <div class="meun-title">课表信息</div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/teacher/scheduleMes.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_user_grey.png">查看课表</a>
    </div>
</div>
