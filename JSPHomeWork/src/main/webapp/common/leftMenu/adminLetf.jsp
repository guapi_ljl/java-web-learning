<%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/16
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>
    .leftMeun{
        overflow: auto;
    }
    .leftMeun::-webkit-scrollbar{
        width: 1px;
        height: 5px;
    }
</style>

<%--    userName=cookie.get("userName").value;--%>
<%--    userId=cookie.get("userId").value;--%>
<%--    chose=cookie.get("chose").value;--%>
<%--<%--%>
<%--    Cookie[] cookies = request.getCookies();--%>
<%--    Object attribute = request.getAttribute("userName");--%>
<%--    System.out.println(attribute);--%>
<%--%>--%>
<%
    Cookie[] cookies = request.getCookies();
    for (Cookie cookie : cookies) {
        if (cookie.getName().equals("userName")){
            request.setAttribute("userName",cookie.getValue());
        }
        if(cookie.getName().equals("userId")){
            request.setAttribute("userId",cookie.getValue());
        }
        if (cookie.getName().equals("userChose")){
            request.setAttribute("userChose",cookie.getValue());
        }
    }
%>

<div class="leftMeun" id="leftMeun" >
    <div id="logoDiv" >
        <p id="logoP">
            <img id="logo" src="${pageContext.request.contextPath}/images/logo1.png"><span>${userName}(管理员)</span>
        </p>
    </div>
    <div id="personInfor">
        <p id="showtime"></p>
        <p id="zn"></p>
        <p>
            <a href="${pageContext.request.contextPath}/exit.jsp">退出登录</a>
        </p>
    </div>
    <div class="meun-title">系统管理</div>
    <div class="meun-item" >
        <a href="${pageContext.request.contextPath}/admin/adminMes.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_card_grey.png">管理员信息</a>
    </div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/admin/adminModified.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_rule_grey.png">修改密码</a>
    </div>
    <div class="meun-title">课程管理</div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/admin/courseMes.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_user_grey.png">课程信息</a>
    </div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/admin/courseAdd.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_change_grey.png">课程录入</a>
    </div>
    <div class="meun-title">教师管理</div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/admin/teacherMes.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_card_grey.png">教师信息</a>
    </div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/admin/teacherAdd.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_change_grey.png">教师录入</a>
    </div>
    <div class="meun-title">课表管理</div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/admin/scheduleMes.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_card_grey.png">课表信息</a>
    </div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/admin/scheduleAdd.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_change_grey.png">课表录入</a>
    </div>
    <div class="meun-title">学生管理</div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/admin/studentMes.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_card_grey.png">学生信息</a>
    </div>
    <div class="meun-item">
        <a href="${pageContext.request.contextPath}/admin/studentAdd.jsp?userName=${userName }&userId=${userId }"><img src="${pageContext.request.contextPath}/images/icon_change_grey.png">学生录入</a>
    </div>
</div>
