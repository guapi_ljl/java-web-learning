<%@ page import="com.chooseCourses.dao.BaseDao" %>
<%@ page import="com.mysql.jdbc.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/16
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="rightContent">
    <a class="toggle-btn" id="nimei">
        <i class="glyphicon glyphicon-align-justify"></i>
    </a>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- 头 -->
        <div role="tabpanel" class="tab-pane active" id="sour">
            <div class="check-div form-inline">
                <center><span style="font-size: 48px;">学生选课系统</span></center>
            </div>
            <!-- java代码 -->
            <%
                //创建rs,ps对象
                PreparedStatement ps;
                ResultSet rs;

                //创建数据库对象
                BaseDao baseDao = new BaseDao();
                //创建分页显示
                int page_count;//总页数
                int p;//当前页数
                int size = 10;//每页记录数
                int result_count;//总记录数
                int begin;//当前页第一条记录索引号
                //换页后获取
                String page_s = request.getParameter("page");
                if(page_s == null){
                    page_s = "1";//设置页码为1
                }
                p = Integer.parseInt(page_s);//转换成int类型

                //sql语句,遍历整个admin表的总数
//                	String sql = "select count(*) from kebiao k,kecheng c,teacher l where k.courseId=c.id and k.teacherId=l.id";
                String sql="select count(*) from schedule s, course c,teacher t where s.courseid=c.id and s.teacherid=t.id";
                //调用DB里面的方法


                rs = baseDao.getConnection().prepareStatement(sql).executeQuery();
                rs.next();

                result_count = rs.getInt(1);//总记录数
                page_count = (result_count + size - 1)/size;//计算总页数
                sql = "select s.id,c.id courseid,c.name,c.room,s.time,t.name teachername from schedule s,course c,teacher t where s.courseid=c.id and s.teacherid=t.id order by s.id asc";
                begin = (p-1)*size;//当前索引
//                System.out.println(begin);
//                System.out.println(size);
                rs = baseDao.getPage(sql, begin, size);//调用分页的方法
                //执行sql语句,并获得返回的结果集

                //三表联合查询
//                	sql = "select k.id,c.bianhao,c.name,c.keshi,k.time,k.jieci,k.room,l.name " +
//							"from kebiao k,kecheng c,teacher l " +
//							"where k.courseId=c.id and k.teacherId=l.id order by k.id asc";

            %>

            <!-- 显示管理员信息 -->
            <div class="data-div">
                <div class="row tableHeader">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 ">序号</div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 ">课程编号</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">课程名称</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">上课时间</div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 ">上课地点</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">授课老师</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">操作</div>
                </div>

                <div class="tablebody">

                    <%
                        //遍历结果集
                        while(rs.next()){
                    %>
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 levl3 ">
                            <%=rs.getInt("id") %>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <span class=""> &nbsp;</span><span><%=rs.getString("courseid") %></span>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <%=rs.getString("name") %>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <%=rs.getString("time") %>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <%=rs.getString("room") %>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <%=rs.getString("teachername") %>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <button class="btn btn-success btn-xs"><a style="color: white" href="scheduleAdd.jsp?userName=${userName}&userId=${userId}">添加</a></button>
                            <button class="btn btn-danger btn-xs"><a style="color: white" href="${pageContext.request.contextPath}/ScheduleDelete?deleteId=<%=rs.getInt("id")%>&userName=${userName}&userId=${userId}">删除</a></button>
                        </div>
                    </div>
                    <%

                        }
                        baseDao.closeResource(null,null,rs);//每次取完数据要记得关闭数据流

                    %>
                </div>
            </div>

            <!--页码块-->
            <footer class="footer">
                <ul class="pagination">
                    <li class="gray">共<%=page_count %>页</li>
                    <li>
                        <%
                            if(p > 1){
                        %>

                        <a href="courseMes.jsp?userName=${userName }&userId=${userId}&page=<%=(p-1)%>"><i class="glyphicon glyphicon-menu-left"> </i></a>
                        <%
                            }
                        %>
                    </li>
                    <li>
                        <%
                            if(p < page_count){
                        %>
                        <a href="courseMes.jsp?userName=${userName }&userId=${userId}&page=<%=(p-1)%>"><i class="glyphicon glyphicon-menu-left"> </i></a>
                        <%
                            }
                        %>
                    </li>
                </ul>
            </footer>
        </div>
    </div>
</div>
