<%@ page language="java" contentType="text/html;charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>登录页面</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>


    <script>
        //判断输入是否为空
        function Login() {
            //获取输入框的内容
            var userName = $('#userName').val();
            var userPw = $('#userPassword').val();

            if (userName == '' || userPw == '') {
                alert('用户名或密码不能为空！！！');
            } else {
                document.getElementById("loginForm").submit();
            }

        }
    </script>
</head>
<body>
<style>
    .main-body {

        position: absolute;
        top: 30%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .form-group {
        width: 300px;
        position: relative;
        left: 20%;
    }

    .inputMsg {
        display: inline;
    }

</style>

<div class="main-body">

    <div style="background-color:whitesmoke;  width:500px; ">
        <form class="form-horizontal" action="Login" method="post" id="loginForm" name="loginForm">
            <h2
                    class="login-title" style="text-align: center">jsp选课系统登录页面</h2>
            <br>

            <div class="form-group">
                <div class="inputMsg">
                    <label>用户名：</label>
                </div>
                <div class="inputMsg" style="float: right">
                    <input id="loginId" name="loginId" placeholder="输入用户名">
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="inputMsg">
                    <label>密码：</label>
                </div>
                <div class="inputMsg" style="float: right">
                    <input type="password" id="loginPassword" name="loginPassword" placeholder="输入登录密码">
                </div>
            </div>

            <br>


            <div class="form-group">
                <label>身&nbsp;&nbsp;份：</label>

                <%--@declare id="admin"--%><%--@declare id="teacher"--%><%--@declare id="student"--%>
                <label class="control-label" for="admin">
                    <input type="radio" name="chose" value="admin" checked="checked">管理员</label>
                <label class="control-label" for="teacher">
                    <input type="radio" name="chose" value="teacher">教师</label>
                <label class="control-label" for="student">
                    <input type="radio" name="chose" value="student">学生</label>
            </div>


            <br>
            <div>
                <span style="color:red;">${ message }</span>
            </div>
            <br>

            <div style="text-align:center">
                <button type="reset">重置</button>
                <button type="button" onclick="Login();">登录</button>
            </div>
        </form>
    </div>
</div>

</body>
</html>