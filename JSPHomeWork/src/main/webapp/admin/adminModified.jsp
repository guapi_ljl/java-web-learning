<%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/16
  Time: 20:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.chooseCourses.dao.BaseDao,java.sql.PreparedStatement" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>adminEdi</title>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <!-- 时间显示 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/common/time.js" charset="utf-8"></script>


    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slide.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flat-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.nouislider.css">
</head>
<body>
<div id="wrap">
    <!-- 左侧菜单栏目块 -->
    <%@include file="../common/leftMenu/adminLetf.jsp" %>
    <!-- 右侧具体内容栏目 -->
    <div id="rightContent">
        <a class="toggle-btn" id="nimei">
            <i class="glyphicon glyphicon-align-justify"></i>
        </a>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- 头 -->
            <div role="tabpanel" class="tab-pane active" id="sour">
                <div class="check-div form-inline">
                    <center>
                        <span style="font-size: 48px;">学生选课系统</span>
                    </center>
                </div>
                <div style="padding: 50px 0; margin-top: 50px; background-color: #fff; text-align: right; width: 420px; margin: 50px auto;">
                    <form class="form-horizontal" name="upForm" id="upForm" action="${pageContext.request.contextPath}/AdminModify" method="post">
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">用户名：</label>
                            <div class="col-xs-5">
                                <input type="text" class="form-control input-sm duiqi" value="${userName}" disabled="true" placeholder="" style="margin-top: 7px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">原密码：</label>
                            <div class="col-xs-5">
                                <input type="password" class="form-control input-sm duiqi" name="oldPw" id="oldPw" placeholder="" style="margin-top: 7px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">新密码：</label>
                            <div class="col-xs-5">
                                <input type="password" class="form-control input-sm duiqi" name="newPw" id="newPw"  placeholder="" style="margin-top: 7px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">重复密码：</label>
                            <div class="col-xs-5">
                                <input type="password" class="form-control input-sm duiqi" name="repeatPw" id="repeatPw"    placeholder="" style="margin-top: 7px;">
                                <input type="hidden" class="form-control input-sm duiqi" name="userId" id="userId"    value="${userId}" style="margin-top: 7px;">
                                <input type="hidden" class="form-control input-sm duiqi" name="userName" id="userName"    value="${userName}" style="margin-top: 7px;">
                            </div>
                        </div>
                        <div>
                            <span style="color:red;">${ message }</span>
                        </div>
                        <div class="form-group text-right">
                            <div class="col-xs-offset-4 col-xs-5"
                                 style="margin-left: 169px;">
                                <button type="reset" class="btn btn-xs btn-white">取 消</button>
                                <button type="button" onclick="Up();" class="btn btn-xs btn-green">保存</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
</body>
<script>
    //判断输入是否为空
    //注意:为预防页面加载不完全,放在body后
    function Up() {
        //获取输入框的内容
        var oldPw = $('#oldPw').val();
        var newPw = $('#newPw').val();
        var repeatPw = $('#repeatPw').val();

        if (oldPw == '' || newPw == '' || repeatPw == '') {
            alert('密码不能为空！！！');
        }
        else if(repeatPw != newPw){
            alert('两次密码不一样！！！');
        }else {
            document.getElementById("upForm").submit();
        }

    }
</script>
</html>