<%@ page import="com.chooseCourses.dao.BaseDao" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/17
  Time: 21:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>添加课表</title>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>


    <!-- 时间显示 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/common/time.js" ></script>


    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slide.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flat-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.nouislider.css">
</head>
<body>
<div id="wrap">
    <!-- 左侧菜单栏目块 -->
    <%@ include file="../common/leftMenu/adminLetf.jsp"%>

    <%
        //遍历课程表和老师表,用于下拉列表框
        //创建db对象
        BaseDao baseDao = new BaseDao();
        PreparedStatement ps;
        ResultSet rs;

        //sql语句
//        String sql = "select * from schedule";
        String sql = "select * from course";
        //调用查询方法
        ps = baseDao.getConnection().prepareStatement(sql);
        //执行sql语句
        rs = ps.executeQuery();//接收结果集
//        System.out.println(request.getParameter("userId"));
//        System.out.println(request.getParameter("userName"));


    %>

    <!-- 右侧具体内容栏目 -->
    <div id="rightContent">
        <a class="toggle-btn" id="nimei">
            <i class="glyphicon glyphicon-align-justify"></i>
        </a>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- 头 -->
            <div role="tabpanel" class="tab-pane active" id="sour">
                <div class="check-div form-inline">
                    <center>
                        <span style="font-size: 48px;">学生选课系统</span>
                    </center>
                </div>
                <div style="padding: 50px 0; margin-top: 50px; background-color: #fff; text-align: right; width: 420px; margin: 50px auto;">
                    <form class="form-horizontal" name="addForm" id="addForm" action="${pageContext.request.contextPath}/ScheduleAdd?&userName=${userName}&userId=${userId}" method="post">
                        <div class="form-group">
                            <label  class="col-xs-4 control-label">序&nbsp;&nbsp;号：</label>
                            <div class="col-xs-4">
                                <select id="Id" style="height:30px; margin-top: 4px;" name="Id" class="form-control duiqi">
                                    <%
                                        //此处还有优化空间，应该id设置成课表中的id，由于设置了主键，id不能重复，但是此处的id显示的是课程id
                                        //遍历全部课程
                                        while(rs.next()){
                                    %>
                                    <option value="<%=rs.getInt("id")%>">
                                        <%= rs.getString("id")%>
                                    </option>
                                    <%
                                        }
                                    %>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-xs-4 control-label">课&nbsp;&nbsp;程：</label>
                            <div class="col-xs-4">
                                <select id="courseName" style="height:30px; margin-top: 4px;" name="courseName" class="form-control duiqi">

                                    <%
                                        rs = ps.executeQuery();//接收结果集
                                        //遍历全部课程
                                        while(rs.next()){
                                    %>
                                    <option value="<%=rs.getString("name")%>">
                                        <%=rs.getString("name") %>
                                    </option>
                                    <%
                                        }
                                    %>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-xs-4 control-label">时&nbsp;&nbsp;间：</label>
                            <div class="col-xs-4">
                                <select id="time" style="height:30px; margin-top: 4px;" name="time" class="form-control duiqi">
                                    <option value="星期一">星期一</option>
                                    <option value="星期二">星期二</option>
                                    <option value="星期三">星期三</option>
                                    <option value="星期四">星期四</option>
                                    <option value="星期五">星期五</option>
                                    <option value="星期六">星期六</option>
                                    <option value="星期日">星期日</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-xs-4 control-label">上课教室：</label>
                            <div class="col-xs-4">
                                <select id="room" style="height:30px; margin-top: 4px;" name="room" class="form-control duiqi">
                                    <option value="多媒体教室1">多媒体教室1</option>
                                    <option value="多媒体教室2">多媒体教室2</option>
                                    <option value="多媒体教室3">多媒体教室3</option>
                                    <option value="多媒体教室4">多媒体教室4</option>
                                    <option value="多媒体教室5">多媒体教室5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-xs-4 control-label">授课老师：</label>
                            <div class="col-xs-4">
                                <select id="teacherId" style="height:30px; margin-top: 4px;" name="teacherId" class="form-control duiqi">
                                    <%
                                        //遍历教师数据
                                        sql = "select * from teacher";
                                        ps = baseDao.getConnection().prepareStatement(sql);
                                        rs = ps.executeQuery();
                                        while(rs.next()){
                                    %>
                                    <option value="<%=rs.getInt("id")%>"><%=rs.getString("name") %></option>
                                    <%
                                        }
                                        baseDao.closeResource(null,ps,rs);//关闭数据流
                                    %>
                                </select>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <div class="col-xs-offset-4 col-xs-5"
                                 style="margin-left: 169px;">
                                <button type="reset" class="btn btn-xs btn-white">取 消</button>
                                <button type="button" onclick="Add();" class="btn btn-xs btn-green">保存</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
</body>
<script>
    //判断输入是否为空
    //注意:为预防页面加载不完全,放在body后
    function Add() {
        //获取输入框的内容
        var courseName = $('#courseName').val();
        var time = $('#time').val();
        var room = $('#room').val();
        var teacherId = $('#teacherId').val();
        var Id = $('#Id').val();

        if (courseName == '' || time == '' || room == '' || teacherId == ''|| Id == '') {
            alert('请填写完整！！！');
        }else {
            document.getElementById("addForm").submit();
        }

    }
</script>
</html>
