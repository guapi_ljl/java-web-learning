<%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/11
  Time: 8:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注销页面</title>
</head>
<body>
<% session.invalidate(); %>
<%--jsp的重定向语句，将页面跳转到了login.jsp--%>
<jsp:forward page="login.jsp"></jsp:forward>
</body>
</html>
