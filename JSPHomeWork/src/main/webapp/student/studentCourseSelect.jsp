<%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/19
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.sql.*,com.chooseCourses.*" %>
<%@ page import="com.chooseCourses.dao.BaseDao" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>学生选课页面</title>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script>
        $(function() {
            $(".meun-item").click(function() {
                $(".meun-item").removeClass("meun-item-active");
                $(this).addClass("meun-item-active");
                var itmeObj = $(".meun-item").find("img");
                itmeObj.each(function() {
                    var items = $(this).attr("src");
                    items = items.replace("_grey.png", ".png");
                    items = items.replace(".png", "_grey.png")
                    $(this).attr("src", items);
                });
                var attrObj = $(this).find("img").attr("src");
                ;
                attrObj = attrObj.replace("_grey.png", ".png");
                $(this).find("img").attr("src", attrObj);
            });
            $("#topAD").click(function() {
                $("#topA").toggleClass(" glyphicon-triangle-right");
                $("#topA").toggleClass(" glyphicon-triangle-bottom");
            });
            $("#topBD").click(function() {
                $("#topB").toggleClass(" glyphicon-triangle-right");
                $("#topB").toggleClass(" glyphicon-triangle-bottom");
            });
            $("#topCD").click(function() {
                $("#topC").toggleClass(" glyphicon-triangle-right");
                $("#topC").toggleClass(" glyphicon-triangle-bottom");
            });
            $(".toggle-btn").click(function() {
                $("#leftMeun").toggleClass("show");
                $("#rightContent").toggleClass("pd0px");
            })
        })
    </script>

    <!-- 时间显示 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/common/time.js" charset="utf-8"></script>


    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slide.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flat-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.nouislider.css">
</head>
<body>
<div id="wrap">
    <!-- 左侧菜单栏目块 -->
    <%@include file="../common/leftMenu/studentLeft.jsp" %>

    <!-- 右侧具体内容栏目 -->
    <div id="rightContent">
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- 头 -->
            <div role="tabpanel" class="tab-pane active" id="sour">
                <div class="check-div form-inline">
                    <center><span style="font-size: 48px;">学生选课系统</span></center>
                </div>

                <!-- java代码 -->
                <%
                    //创建rs,ps对象
                    PreparedStatement ps;
                    ResultSet rs;

                    //创建数据库对象
                    BaseDao baseDao = new BaseDao();

                    //创建分页显示
                    int page_count;//总页数
                    int p;//当前页数
                    int size = 10;//每页记录数
                    int result_count;//总记录数
                    int begin;//当前页第一条记录索引号
                    //换页后获取
                    String page_s = request.getParameter("page");
                    if(page_s == null){
                        page_s = "1";//设置页码为1
                    }
                    p = Integer.parseInt(page_s);//转换成int类型

                    //sql语句,遍历整个admin表的总数
                    String sql = "select count(*) from schedule s ,course c,teacher t where s.courseid=c.id and s.teacherid=t.id";
                    //调用DB里面的方法
                    ps = baseDao.getConnection().prepareStatement(sql);
                    //执行sql语句,并获得返回的结果集
                    rs = ps.executeQuery();
                    rs.next();
                    result_count = rs.getInt(1);//总记录数
                    page_count = (result_count + size - 1)/size;//计算总页数
                    //三表联合查询
                    //sql = "select k.id,c.bianhao,c.mingcheng,c.keshi,k.shijian,k.jieci,k.jiaoshi,l.xingming from kebiao k,kecheng c,laoshi l where k.kecheng_id=c.id and k.laoshi_id=l.id order by k.id asc";
                    sql = "select s.id,c.id courseid,c.name,s.time,s.room,t.name teachername from schedule s,course c,teacher t where s.courseid=c.id and s.teacherid=t.id order by s.id asc";
                    begin = (p-1)*size;//当前索引
                    rs = baseDao.getPage(sql, begin, size);//调用分页的方法

                %>

                <!-- 显示管理员信息 -->
                <div class="data-div">
                    <div class="row tableHeader">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 ">课表序号</div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 ">课程编号</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">课程名称</div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 ">上课时间</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">上课教室</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">授课老师</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">操作</div>
                    </div>

                    <div class="tablebody">

                        <%
                            //遍历结果集
                            while(rs.next()){

                        %>
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 levl3 ">
                                <%=rs.getInt("id") %>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1  levl3">
                                <span class=""> &nbsp;</span><span><%=rs.getString("courseid") %></span>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <%=rs.getString("name") %>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 levl3 ">
                                <%=rs.getString("time") %>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <%=rs.getString("room") %>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <%=rs.getString("teachername") %>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <button class="btn btn-danger btn-xs"><a style="color: white;" href="${pageContext.request.contextPath}/ScheduleSelect?id=<%=rs.getInt("id")%>&userName=${userName}&userId=${userId}">选择课程</a></button>
                                &nbsp;
                            </div>
                        </div>
                        <%

                            }
                            baseDao.closeResource(null,ps,rs);//每次取完数据要记得关闭数据流

                        %>
                    </div>
                </div>

                <!--页码块-->
                <footer class="footer">
                    <ul class="pagination">
                        <li class="gray">共<%=page_count %>页</li>
                        <li>
                            <%
                                if(p > 1){
                            %>

                            <a href="studentCourseSelect.jsp?userName=${userName}&userId=${userId}&page=<%=(p-1)%>"><i class="glyphicon glyphicon-menu-left"> </i></a>
                            <%
                                }
                            %>
                        </li>
                        <li>
                            <%
                                if(p < page_count){
                            %>
                            <a href="studentCourseSelect.jsp?userName=${userName}&userId=${userId}&page=<%=(p+1)%>"><i class="glyphicon glyphicon-menu-right"> </i></a>
                            <%
                                }
                            %>
                        </li>
                    </ul>
                </footer>

            </div>
        </div>
    </div>
</div>
</body>
</html>