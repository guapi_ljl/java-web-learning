<%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/20
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>选课失败</title>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    <!-- 时间显示 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/common/time.js" charset="utf-8"></script>


    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slide.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flat-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.nouislider.css">
</head>
<body>
<div id="wrap">
    <!-- 左侧菜单栏目块 -->
    <%@include file="../common/leftMenu/studentLeft.jsp" %>

    <!-- 右侧具体内容栏目 -->
    <div id="rightContent">
        <a class="toggle-btn" id="nimei">
            <i class="glyphicon glyphicon-align-justify"></i>
        </a>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- 头 -->
            <div role="tabpanel" class="tab-pane active" id="sour">
                <div class="check-div form-inline">
                    <center>
                        <span style="font-size: 48px;">学生选课系统</span>
                    </center>
                </div>
                <div style="padding: 50px 0; margin-top: 50px; background-color: #fff; text-align: right; width: 420px; margin: 50px auto;">
                    <form class="form-horizontal" name="upForm" id="upForm" action="${pageContext.request.contextPath}/studentUpdate" method="post">


                        <div class="form-group text-right">
                            <label  class="col-xs-8 control-label">选课失败!你已选择该课程。</label>
                            <div class="col-xs-offset-4 col-xs-8" style="margin-left: -12px;">
                                &nbsp;&nbsp;
                            </div>
                            <div class="col-xs-offset-4 col-xs-8" style="margin-left: -12px;">
                                <button type="button" class="btn btn-xs btn-white"><a href="studentCourseMes.jsp?userName=${userName}&userId=${userId}">查看课表</a></button>
                                <button type="button" class="btn btn-xs btn-green"><a style="color: white" href="studentCourseSelect.jsp?userName=${userName}&userId=${userId}">继续选课</a></button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
</body>
</html>