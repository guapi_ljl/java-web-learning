<%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/19
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.chooseCourses.*,java.sql.*" %>
<%@ page import="com.chooseCourses.dao.BaseDao" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>学生信息</title>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <!-- 时间显示 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/common/time.js" charset="utf-8"></script>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slide.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flat-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.nouislider.css">
</head>
<body>
<div id="wrap">
    <!-- 左侧菜单栏目块 -->
    <%@include file="../common/leftMenu/studentLeft.jsp" %>

    <!-- 右侧具体内容栏目 -->
    <div id="rightContent">
        <a class="toggle-btn" id="nimei">
            <i class="glyphicon glyphicon-align-justify"></i>
        </a>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- 头 -->
            <div role="tabpanel" class="tab-pane active" id="sour">
                <div class="check-div form-inline">
                    <center>
                        <span style="font-size: 48px;">学生选课系统</span>
                    </center>
                </div>
                <!-- java代码 -->
                <%
                    //创建rs,ps对象
                    PreparedStatement ps;
                    ResultSet rs;

                    //创建数据库对象
                    BaseDao baseDao = new BaseDao();
                    //创建sql语句
                    String sql = "select * from student where id=" + request.getParameter("userId");
                    //调用查询方法
                    ps = baseDao.getConnection().prepareStatement(sql);
                    //接收结果集
                    rs = ps.executeQuery();
                    //下面遍历结果集,在需要显示的地方

                %>
                <div style="padding: 50px 0; margin-top: 50px; background-color: #fff; text-align: right; width: 420px; margin: 50px auto;">
                    <form class="form-horizontal" name="upForm" id="upForm" action="${pageContext.request.contextPath}/UpAdmin" method="post">
                        <%
                            while(rs.next()){
                        %>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">学&nbsp;&nbsp;号：</label>
                            <div class="col-xs-5">
                                <input type="text" class="form-control input-sm duiqi" value="<%=rs.getString("Id") %>" readonly="readonly" placeholder="" style="margin-top: 7px;color:black;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">姓&nbsp;&nbsp;名：</label>
                            <div class="col-xs-5">
                                <input type="" class="form-control input-sm duiqi" value="<%=rs.getString("name") %>" name="name" id="name" readonly="readonly" placeholder="" style="margin-top: 7px;color:black;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">性&nbsp;&nbsp;别：</label>
                            <div class="col-xs-5 text-left">
                                <%
                                    //判断一下性别
                                    if("男".equals(rs.getString("sex"))){
                                %>
                                <label class="control-label" > <input disabled="disabled" type="radio" name="sex" id="sex" value="男" checked="checked"> 男
                                </label>
                                &nbsp;&nbsp;&nbsp;
                                <label class="control-label" > <input disabled="disabled" type="radio" name="sex" id="sex" value="女"> 女
                                </label>
                                <%
                                }else{
                                %>
                                <label class="control-label" > <input disabled="disabled" type="radio" name="sex" id="sex" value="男" > 男
                                </label>
                                &nbsp;&nbsp;&nbsp;
                                <label class="control-label" > <input disabled="disabled" type="radio" name="sex" id="sex" value="女" checked="checked"> 女
                                </label>
                                <%
                                    }
                                %>
                            </div>
                        </div>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">年&nbsp;&nbsp;龄：</label>
                            <div class="col-xs-5">
                                <input type="" class="form-control input-sm duiqi" value="<%=rs.getString("age") %>" name="age" id="age" readonly="readonly" placeholder="" style="margin-top: 7px;color:black;">
                                <input type="hidden" class="form-control input-sm duiqi" name="userId" id="userId"    value="${userId}" style="margin-top: 7px;">
                                <input type="hidden" class="form-control input-sm duiqi" name="userName" id="userName"    value="${userName}" style="margin-top: 7px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">班&nbsp;&nbsp;级：</label>
                            <div class="col-xs-5">
                                <input type="" class="form-control input-sm duiqi" value="<%=rs.getString("classes") %>" name="classes" id="classes" readonly="readonly" placeholder="" style="margin-top: 7px;color:black;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">登陆帐号：</label>
                            <div class="col-xs-5">
                                <input type="" class="form-control input-sm duiqi" value="<%=rs.getString("loginId") %>" name="loginId" id="loginId" readonly="readonly" placeholder="" style="margin-top: 7px;color:black;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label   class="col-xs-4 control-label">登陆密码：</label>
                            <div class="col-xs-5">
                                <input type="" class="form-control input-sm duiqi" value="<%=rs.getString("loginpassword") %>" name="loginpassword" id="loginpassword" readonly="readonly" placeholder="" style="margin-top: 7px;color:black;">
                            </div>
                        </div>
                        <%
                            }
                            baseDao.closeResource(null,ps,rs);//关闭数据流
                        %>

                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
</body>
</html>
