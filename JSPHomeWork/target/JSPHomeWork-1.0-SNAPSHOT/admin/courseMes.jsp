<%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/16
  Time: 15:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,com.chooseCourses.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>课程信息</title>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>


    <!-- 时间显示 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/common/time.js" charset="utf-8"></script>


    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/common.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slide.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flat-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.nouislider.css">
</head>
<body>
<div id="wrap">
    <!-- 左侧菜单栏目块 -->
    <%@include file="../common/leftMenu/adminLetf.jsp" %>

    <!-- 右侧具体内容栏目 -->

    <%@include file="courseData.jsp"%>
</div>
</body>
<script>
    //判断输入是否为空
    //注意:为预防页面加载不完全,放在body后
    function Edi() {
        //获取输入框的内容
        var room = $('#room').val();
        var name = $('#name').val();
        var courseId = $('#couseId').val();


        if (courseId == '' || name == '' || room == '') {
            alert('请填写完整！！！');
        } else {
            document.getElementById("formEdi").submit();
        }

    }
</script>
</html>