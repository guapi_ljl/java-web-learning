<%--
  Created by IntelliJ IDEA.
  User: 瓜皮哦
  Date: 2021/10/16
  Time: 17:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.chooseCourses.dao.BaseDao,java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>

<div id="rightContent">
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- 头 -->
        <div role="tabpanel" class="tab-pane active" id="sour">
            <div class="check-div form-inline">
                <center><span style="font-size: 48px;">学生选课系统</span></center>
            </div>

            <!-- java代码 -->
            <%
                //创建rs,ps对象
                PreparedStatement ps;
                ResultSet rs;

                //创建数据库对象
                BaseDao baseDao = new BaseDao();

                //创建分页显示
                int page_count;//总页数
                int p;//当前页数
                int size = 10;//每页记录数
                int result_count;//总记录数
                int begin;//当前页第一条记录索引号
                //换页后获取
                String page_s = request.getParameter("page");
                if(page_s == null){
                    page_s = "1";//设置页码为1
                }
                p = Integer.parseInt(page_s);//转换成int类型

                //sql语句,遍历整个admin表的总数
                String sql = "select count(*) from course";
                //调用DB里面的方法
                ps = baseDao.getConnection().prepareStatement(sql);
                //执行sql语句,并获得返回的结果集
                rs = ps.executeQuery();
                rs.next();
                result_count = rs.getInt(1);//总记录数
                page_count = (result_count + size - 1)/size;//计算总页数
                sql = "select * from course order by id";
                begin = (p-1)*size;//当前索引
                rs = baseDao.getPage(sql, begin, size);//调用分页的方法

            %>

            <!-- 显示管理员信息 -->
            <div class="data-div">
                <div class="row tableHeader">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 ">序号</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">课程名称</div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">上课教室</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">操作</div>
                </div>

                <div class="tablebody">

                    <%
                        //遍历结果集
                        while(rs.next()){
                            String editId = rs.getString("id");
                            request.setAttribute("editId",editId);

                    %>
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 levl3 ">
                            <%=rs.getInt("id") %>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <%=rs.getString("name") %>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <%=rs.getString("room") %>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#changeSource">编辑</button>
                            <button class="btn btn-danger btn-xs"><a style="color: white" href="${pageContext.request.contextPath}/CourseDelete?id=<%=rs.getInt("id")%>&userName=${userName}&userId=${userId}">删除</a></button>
                        </div>
                    </div>
                    <%

                        }
                        baseDao.closeResource(null,ps,rs);//每次取完数据要记得关闭数据流

                    %>
                </div>
            </div>

            <!--页码块-->
            <footer class="footer">
                <ul class="pagination">
                    <li class="gray">共<%=page_count %>页</li>
                    <li>
                        <%
                            if(p > 1){
                        %>

                        <a href="courseMes.jsp?userName=${userName}&userId=${userId}&page=<%=(p-1)%>"><i class="glyphicon glyphicon-menu-left"> </i></a>
                        <%
                            }
                        %>
                    </li>
                    <li>
                        <%
                            if(p < page_count){
                        %>
                        <a href="courseMes.jsp?userName=${userName}&userId=${userId}&page=<%=(p+1)%>"><i class="glyphicon glyphicon-menu-right"> </i></a>
                        <%
                            }
                        %>
                    </li>
                </ul>
            </footer>
            <%--权限判断代码尚未通过测试--%>
<%--            <% if(request.getAttribute("chose").equals("admin")){%>--%>
            <!--编辑课程弹出窗口-->
                <div class="modal fade" id="changeSource" role="dialog" aria-labelledby="gridSystemModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="gridSystemModalLabel">编辑课程信息</h4>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <form class="form-horizontal" id="formEdi" action="${pageContext.request.contextPath}/CourseEdit?editId=${editId}&userName=${userName}&userId=${userId}" method="post">
                                    <div class="form-group">
                                        <label  class="col-xs-3 control-label">课程编号：</label>
                                        <div class="col-xs-8 ">
                                            <input type="" class="form-control input-sm duiqi" name="courseId" id="courseId" >
                                            <input type="hidden" class="form-control input-sm duiqi" name="userName" id="userName" value="${userName}">
                                            <input type="hidden" class="form-control input-sm duiqi" name="userId" id="userId" value="${userId}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="col-xs-3 control-label">课程名称：</label>
                                        <div class="col-xs-8 ">
                                            <input type="" class="form-control input-sm duiqi" name="name" id="name" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="col-xs-3 control-label">上课地点：</label>
                                        <div class="col-xs-8 ">
                                            <input type="" class="form-control input-sm duiqi" name="room" id="room" >
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
                            <button type="button" onclick="Edi();" class="btn btn-xs btn-green">保 存</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
<%--            <%}else{--%>
<%--                response.sendRedirect("error.jsp");--%>
<%--            }%>--%>


        </div>
    </div>
</div>